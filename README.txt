commerce_svea - Drupal 7/Commerce  payment method for SveaWebPay.
=================================================================

This module implements SveaWebPay as payment method for Drupal Commerce, version 
7.x-1.4 and higher. The recommended method to install Drupal Commerce is to use 
the Commerce Kickstart (http://drupal.org/project/commerce_kickstart).


Install
-------
Install and enable this module as a normal Drupal module. Prefarable in 
sites/all/modules

Go into payment settings and fill in your Merchant ID and Secret word. Save and 
your installation connects to SveaWebPay and picks up some values from there.

As normal drupal Commerce behavior an fully payed order is does not change status 
to completed. To made that you must go into rules and change.

Secure pages and SSL
--------------------
All E-commerce sites should have SSL enabled. For this module it's a 
requirement to be able to enable invoices. If you don't want to enable SSL 
for the whole site do it at least for cart/*. 
To be able to enable Invoices must the modules Secure pages module 
(http://drupal.org/project/securepages) be enabled.

GET or POST in answer from SveaWebPay
-------------------------------------
The answer from SveaWebPay can be posted as GET or as POST. The recommendation 
is to use POST.

Using GET, please note that PHP setups with the suhosin patch installed will 
have a default limit of 512 characters for get parameters. Although bad 
practice, most browsers (including IE) supports URLs up to around 2000 
characters, while Apache has a default of 8000.

To add support for long parameters with suhosin, add 
suhosin.get.max_value_length = <limit> in php.ini

If you are getting MAC errors using GET the reason is probably above, where your
return string from Svea it truncated.

VAT
---
To be able to use invoiceing VAT must be enabled. An errocode 106 from SveaWEbPay
can indicate that VAt is not enabled.

