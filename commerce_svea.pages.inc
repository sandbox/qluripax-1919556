<?php

/**
 * @file
 * SveaWebPay administration tasks.
 */


/**
 * Validate MAC (hash) from SveaWebPay.
 *
 * @param string $data_to_validate
 *   The data to validate.
 * @param string $mac_to_validate
 *   The MAC to validate.
 * @param string $secret_word
 *   The secret word from SveaWebPay.
 *
 * @return bool
 *   TRUE if the data from SveaWebPay is valid.
 */
function _commerce_svea_validatemac($data_to_validate, $mac_to_validate, $secret_word = '') {
  $mac_string = hash("SHA512", $data_to_validate . $secret_word);
  return $mac_to_validate == $mac_string;
}

/**
 * Errorhandler, if $status_code != UC_SVEAWEBPAY_SUCCESS.
 *
 * @param string $status_code
 *   The status code in return from SveaWebPay.
 *
 * @return array
 *   An array containg error code and description.
 */
function _commerce_svea_error_handler($status_code) {
  switch ($status_code) {
    case 100:
      $error = array(
        'name' => 'INTERNAL_ERROR',
        'description' => t('Invalid - contact integrator.'),
      );
      break;

    case 101:
      $error = array(
        'name' => 'XMLPARSEFAIL',
        'description' => t('Invalid XML.'),
      );
      break;

    case 102:
      $error = array(
        'name' => 'ILLEGAL_ENCODING',
        'description' => t('Invalid encoding.'),
      );
      break;

    case 104:
      $error = array(
        'name' => 'ILLEGAL_URL',
        'description' => '',
      );
      break;

    case 105:
      $error = array(
        'name' => 'ILLEGAL_TRANSACTIONSTATUS',
        'description' => t('Invalid transaction status.'),
      );
      break;

    case 106:
      $error = array(
        'name' => 'EXTERNAL_ERROR',
        'description' => t('Failure at third party e.g. failure at the bank.'),
      );
      break;

    case 107:
      $error = array(
        'name' => 'DENIED_BY_BANK',
        'description' => t('Transaction rejected by bank.'),
      );
      break;

    case 108:
      $error = array(
        'name' => 'CANCELLED',
        'description' => t('Transaction cancelled.'),
      );
      break;

    case 109:
      $error = array(
        'name' => 'NOT_FOUND_AT_BANK',
        'description' => t('Transaction not found at the bank.'),
      );
      break;

    case 110:
      $error = array(
        'name' => 'ILLEGAL_TRANSACTIONID',
        'description' => t('Invalid transaction ID.'),
      );
      break;

    case 111:
      $error = array(
        'name' => 'MERCHANT_NOT_CONFIGURED',
        'description' => t('Merchant not configured.'),
      );
      break;

    case 112:
      $error = array(
        'name' => 'MERCHANT_NOT_CONFIGURED_AT_BANK',
        'description' => t('Merchant not configured at the bank.'),
      );
      break;

    case 113:
      $error = array(
        'name' => 'PAYMENTMETHOD_NOT_CONFIGURED',
        'description' => t('Payment method not configured for merchant.'),
      );
      break;

    case 114:
      $error = array(
        'name' => 'TIMEOUT_AT_BANK',
        'description' => t('Timeout at the bank.'),
      );
      break;

    case 115:
      $error = array(
        'name' => 'MERCHANT_NOT_ACTIVE',
        'description' => t('The merchant is disabled.'),
      );
      break;

    case 116:
      $error = array(
        'name' => 'PAYMENTMETHOD_NOT_ACTIVE',
        'description' => t('The payment method is disabled.'),
      );
      break;

    case 117:
      $error = array(
        'name' => 'ILLEGAL_AUTHORIZED_AMOUNT',
        'description' => t('Invalid authorized amount.'),
      );
      break;

    case 118:
      $error = array(
        'name' => 'ILLEGAL_CAPTURED_AMOUNT',
        'description' => t('Invalid captured amount.'),
      );
      break;

    case 119:
      $error = array(
        'name' => 'ILLEGAL_CREDITED_AMOUNT',
        'description' => t('Invalid credited amount.'),
      );
      break;

    case 120:
      $error = array(
        'name' => 'NOT_SUFFICIENT_FUNDS',
        'description' => t('Not enough founds.'),
      );
      break;

    case 121:
      $error = array(
        'name' => 'EXPIRED_CARD',
        'description' => t('The card has expired.'),
      );
      break;

    case 122:
      $error = array(
        'name' => 'STOLEN_CARD',
        'description' => t('Stolen card.'),
      );
      break;

    case 123:
      $error = array(
        'name' => 'LOST_CARD',
        'description' => t('Lost card.'),
      );
      break;

    case 124:
      $error = array(
        'name' => 'EXCEEDS_AMOUNT_LIMIT',
        'description' => t('Amount exceeds the limit.'),
      );
      break;

    case 125:
      $error = array(
        'name' => 'EXCEEDS_FREQUENCY_LIMIT',
        'description' => t('Frequency limit exceeded.'),
      );
      break;

    case 126:
      $error = array(
        'name' => 'TRANSACTION_NOT_BELONGING_TO_MERCHANT',
        'description' => t('Transaction does not belong to merchant.'),
      );
      break;

    case 127:
      $error = array(
        'name' => 'CUSTOMERREFNO_ALREADY_USED',
        'description' => t('Customer reference number already used in another transaction.'),
      );
      break;

    case 128:
      $error = array(
        'name' => 'NO_SUCH_TRANS',
        'description' => t('Transaction does not exist.'),
      );
      break;

    case 129:
      $error = array(
        'name' => 'DUPLICATE_TRANSACTION',
        'description' => t('More than one transaction found for the given customer reference number.'),
      );
      break;

    case 130:
      $error = array(
        'name' => 'ILLEGAL_OPERATION',
        'description' => t('Operation not allowed for the given.'),
      );
      break;

    case 131:
      $error = array(
        'name' => 'COMPANY_NOT_ACTIVE',
        'description' => t('Company inactive.'),
      );
      break;

    case 132:
      $error = array(
        'name' => 'SUBSCRIPTION_NOT_FOUND',
        'description' => t('No subscription exist.'),
      );
      break;

    case 133:
      $error = array(
        'name' => 'SUBSCRIPTION_NOT_ACTIVE',
        'description' => t('Subscription not active.'),
      );
      break;

    case 134:
      $error = array(
        'name' => 'SUBSCRIPTION_NOT_SUPPORTED',
        'description' => t('Payment method doesn’t support subscriptions.'),
      );
      break;

    case 135:
      $error = array(
        'name' => 'ILLEGAL_DATE_FORMAT',
        'description' => t('Illegal date format.'),
      );
      break;

    case 136:
      $error = array(
        'name' => 'ILLEGAL_RESPONSE_DATA',
        'description' => t('Illegal response data.'),
      );
      break;

    case 137:
      $error = array(
        'name' => 'IGNORE_CALLBACK',
        'description' => t('Ignore callback.'),
      );
      break;

    case 138:
      $error = array(
        'name' => 'CURRENCY_NOT_CONFIGURED',
        'description' => t('Currency not configured.'),
      );
      break;

    case 139:
      $error = array(
        'name' => 'CURRENCY_NOT_ACTIVE',
        'description' => t('Currency not active.'),
      );
      break;

    case 140:
      $error = array(
        'name' => 'CURRENCY_ALREADY_CONFIGURED',
        'description' => t('Currency is already configured.'),
      );
      break;

    case 141:
      $error = array(
        'name' => 'ILLEGAL_AMOUNT_OF_RECURS_TODAY',
        'description' => t('Ilegal amount of recurs per day.'),
      );
      break;

    case 142:
      $error = array(
        'name' => 'NO_VALID_PAYMENT_METHODS',
        'description' => t('No valid paymentmethods.'),
      );
      break;

    case 300:
      $error = array(
        'name' => 'BAD_CARDHOLDER_NAME',
        'description' => t('Invalid value for cardholder name.'),
      );
      break;

    case 301:
      $error = array(
        'name' => 'BAD_TRANSACTION_ID',
        'description' => t('Invalid value for transaction id.'),
      );
      break;

    case 302:
      $error = array(
        'name' => 'BAD_REV',
        'description' => t('Invalid value for rev.'),
      );
      break;

    case 303:
      $error = array(
        'name' => 'BAD_MERCHANT_ID ',
        'description' => t('Invalid value for merchant id.'),
      );
      break;

    case 304:
      $error = array(
        'name' => 'BAD_LANG ',
        'description' => t('Invalid value for lang.'),
      );
      break;

    case 305:
      $error = array(
        'name' => 'BAD_AMOUNT ',
        'description' => t('Invalid value for amount.'),
      );
      break;

    case 306:
      $error = array(
        'name' => 'BAD_CUSTOMERREFNO',
        'description' => t('Invalid value for customer refno.'),
      );
      break;

    case 307:
      $error = array(
        'name' => 'BAD_CURRENCY',
        'description' => t('Invalid value for currency.'),
      );
      break;

    case 308:
      $error = array(
        'name' => 'BAD_PAYMENTMETHOD',
        'description' => t('Invalid value for payment method.'),
      );
      break;

    case 309:
      $error = array(
        'name' => 'BAD_RETURNURL',
        'description' => t('Invalid value for return url.'),
      );
      break;

    case 310:
      $error = array(
        'name' => 'BAD_LASTBOOKINGDAY',
        'description' => t('Invalid value for last booking day.'),
      );
      break;

    case 311:
      $error = array(
        'name' => 'BAD_MAC',
        'description' => t('Invalid value for mac.'),
      );
      break;

    case 312:
      $error = array(
        'name' => 'BAD_TRNUMBER',
        'description' => t('Invalid value for tr number.'),
      );
      break;

    case 313:
      $error = array(
        'name' => 'BAD_AUTHCODE',
        'description' => t('Invalid value for authcode.'),
      );
      break;

    case 314:
      $error = array(
        'name' => 'BAD_CC_DESCR',
        'description' => t('Invalid value for cc_descr.'),
      );
      break;

    case 315:
      $error = array(
        'name' => 'BAD_ERROR_CODE',
        'description' => t('Invalid value for error_code.'),
      );
      break;

    case 316:
      $error = array(
        'name' => 'BAD_CARDNUMBER_OR_CARDTYPE_NOT_CONFIGURED',
        'description' => t('Card type not configured for merchant.'),
      );
      break;

    case 317:
      $error = array(
        'name' => 'BAD_SSN',
        'description' => t('Invalid value for ssn.'),
      );
      break;

    case 318:
      $error = array(
        'name' => 'BAD_VAT',
        'description' => t('Invalid value for vat.'),
      );
      break;

    case 319:
      $error = array(
        'name' => 'BAD_CAPTURE_DATE',
        'description' => t('Invalid value for capture date.'),
      );
      break;

    case 320:
      $error = array(
        'name' => 'BAD_CAMPAIGN_CODE_INVALID',
        'description' => t('Invalid value for campaign code. There are no valid matching campaign codes.'),
      );
      break;

    case 321:
      $error = array(
        'name' => 'BAD_SUBSCRIPTION_TYPE',
        'description' => t('Invalid subscription type.'),
      );
      break;

    case 322:
      $error = array(
        'name' => 'BAD_SUBSCRIPTION_ID',
        'description' => t('Invalid subscription id.'),
      );
      break;

    case 323:
      $error = array(
        'name' => 'BAD_BASE64',
        'description' => t('Invalid base64.'),
      );
      break;

    case 324:
      $error = array(
        'name' => 'BAD_CAMPAIGN_CODE',
        'description' => t('Invalid campaign code. Missing value.'),
      );
      break;

    case 325:
      $error = array(
        'name' => 'BAD_CALLBACKURL',
        'description' => t('Invalid callbackurl.'),
      );
      break;

    case 500:
      $error = array(
        'name' => 'ANTIFRAUD_CARDBIN_NOT_ALLOWED',
        'description' => t('Antifraud - cardbin not allowed.'),
      );
      break;

    case 501:
      $error = array(
        'name' => 'ANTIFRAUD_IPLOCATION_NOT_ALLOWED',
        'description' => t('Antifraud – iplocation not allowed.'),
      );
      break;

    default:
      $error = array(
        'name' => 'UNKNOWN',
        'description' => t('Unknown error!'),
      );
  }
  return $error;
}

/**
 * Handles decoding and transforming of XML-response from SveaWebPay.
 *
 * @param array $xml_data
 *   An array containing XML-data.
 * @param bool $encoded
 *   TRUE if XML-data is base64 encoded.
 *
 * @return array
 *   Array containing the corresponding data from XML.
 */
function _commerce_svea_decode_response($xml_data, $encoded = TRUE) {
  $xml = $encoded ? base64_decode($xml_data) : $xml_data;

  // $simpleXml = new SimpleXMLElement($xml);.
  $xml_array = json_decode(json_encode((array) simplexml_load_string($xml)), 1);

  return $xml_array;
}
